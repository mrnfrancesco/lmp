package lmp.tutoring.lessons.second.innerclass;

import javax.swing.JOptionPane;

public class InnerClassTest {
    
    public static void main(String[] args) {
        
        TalkingClock clock = new TalkingClock(1000);
        clock.start();
        
        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
    }
}
