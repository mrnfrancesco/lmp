package lmp.tutoring.lessons.second.innerclass;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.Timer;

public class TalkingClock {
    
    private int interval;
    
    /**
     * Costruisce un orologio sonoro
     * @param interval intervallo di tempo tra i messaggi (in millisecondi)
     */
    public TalkingClock(int interval) {
        this.interval = interval;
    }
    
    public void start() {
        TimePrinter listener = new TimePrinter();
        Timer timer = new Timer(interval, listener);
        timer.start();
    }
    
    private class TimePrinter implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            Date now = new Date();
            System.out.println("the time is " + now);
        }
    }
}
