package lmp.tutoring.lessons.second.exercise;

public class StealDeckGameTest {
    
    public static void main(String[] args) {
        /* ci aspettiamo che l'utente mi dia esattamente due giocatori */
        if(args.length < 2 || args.length > 2) {
            System.err.println("Specify the two players' name");
            System.exit(0);
        }
        /* creiamo il gioco */
        StealDeckGame game = new StealDeckGame(
                new Player(args[0]),
                new Player(args[1])
        );
       /* cominciamo la partita e continuiamo finché non finiscono
        * le carte del mazzo */
        while(!game.isFinished()) {
            game.showTable();
            game.playTurn();
            game.changeTurn();
            game.showTable();
            game.playTurn();
            game.distributeCards(1);
        }
        /* annunciamo il vincitore! Evviva!!! */
        System.out.print("\n\nThe winner is: ");
        Player winner = game.getWinner();
        if(winner != null) {
            System.out.println(winner.toString());
        }
        else {
            System.out.println("none :(");
        }
    }
}
