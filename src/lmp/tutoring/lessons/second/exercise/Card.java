package lmp.tutoring.lessons.second.exercise;

public class Card {
    /** I semi che ogni carta può avere */
    public static enum Seed {SPADES, HEARTS, DIAMONDS, CLUBS}
    
    private final int value;
    private final Seed seed;
    
    /**
     * Crea una carta da gioco.
     * @param value il valore della carta.
     * @param seed il seme della carta.
     */
    public Card(int value, Seed seed) {
        this.value = value;
        this.seed = seed;
    }
    
    /**
     * Confronta il valore di due carte.
     * @param other la carta con cui fare il confronto.
     * @return 1 se la carta corrente vale di più, -1 se vale di meno,
     *          0 se ha lo stesso valore.
     */
    public int compare(Card other) {
        if(this.value > other.value) {
            return 1;
        }
        else if(this.value < other.value) {
            return -1;
        }
        else {
            return 0;
        }
    }
    
    /**
     * Restituisce il valore della carta di gioco.
     * @return il valore della carta.
     */
    public int getValue() {
        return value;
    }

    /**
     * Produce una rappresentazione visuale della carta.
     * @return una stringa che rappresenta graficamente la carta.
     */
    @Override
    public String toString() {
        char card = '\udca0';
        card += '\u0010' * seed.ordinal();
        card += '\u0001' * value;
        return "\ud83c" + card;
    }
    
}