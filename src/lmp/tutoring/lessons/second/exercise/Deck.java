package lmp.tutoring.lessons.second.exercise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class Deck {
    private final int DECK_CAPACITY = 52;
    
    private LinkedList<Card> cards;
    
    /**
     * Crea un mazzo di carte completo e già mischiato.
     */
    public Deck() {
        cards = new LinkedList<>();
        /* copiamo i semi solo per un'ottimizzazione in termini di tempo */
        Card.Seed[] seeds = Card.Seed.values();
        /* aggiungiamo tutte le carte del mazzo */
        for(int i = 0; i < DECK_CAPACITY; ++i) {
            int value = (i % 13) + 1;
            cards.add(new Card(value, seeds[i % 4]));
        }
        /* mischiamo le carte */
        Collections.shuffle(cards);
    }
    
    /**
     * Richiede il numero specificato di carte dal mazzo.
     * @param cardNo il numero di carte richieste.
     * @return il numero di carte specificato se disponibili, tutte le
     *          restanti altrimenti.
     */
    public ArrayList<Card> give(int cardNo) {
        /* controlliamo di avere abbastanza carte, altrimenti diamo
         * tutte le rimanenti */
        if(cardNo > cards.size()) {
            cardNo = cards.size();
        }
        
        final ArrayList<Card> hand = new ArrayList<>(cardNo);
        /* prendiamo il numero richiesto di carte dalla cima del mazzo */
        for(int i = 0; i < cardNo; ++i) {
            hand.add(cards.removeFirst());
        }
        
        return hand;
    }
    
    /**
     * Indica se il mazzo di carte è o no finito.
     * @return true se c'è almeno un'altra carta, false altrimenti.
     */
    public boolean hasNext() {
        return (cards.size() > 0);
    }
}
