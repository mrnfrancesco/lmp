package lmp.tutoring.lessons.second.exercise;

import java.util.ArrayList;

public class Player {
    
    private final String name;
    private ArrayList<Card> hand;
    private int points;
    private int stolenCardNo;

    /**
     * Crea un giocatore con il nome specificato.
     * @param name il nome del giocatore.
     */
    public Player(String name) {
        this.name = name;
        hand = new ArrayList<>();
        /* cominciamo con punteggio nullo */
        points = 0;
        stolenCardNo = 0;
    }

    /**
     * Mette in gioco una carta togliendola dalla propria mano.
     * @param cardIndex l'indice della carta da giocare.
     * @return La carta giocata.
     */
    public Card playCard(int cardIndex) {
        Card choosenCard = hand.get(cardIndex);
        hand.remove(cardIndex);
        return choosenCard;
    }
    
    /**
     * Aggiunge alla propria mano le carte date.
     * @param cards le carte da aggiungere alla propria mano.
     */
    public void takeCards(ArrayList<Card> cards) {
        hand.addAll(cards);
    }

    /**
     * Incrementa il punteggio in base alle carte vinte.
     * @param stolenCard le carte vinte.
     */
    public void raisePoint(ArrayList<Card> stolenCard) {
        for(Card card : stolenCard) {
            points += card.getValue();
            ++stolenCardNo;
        }
    }

    /**
     * Restituisce il punteggio del giocatore.
     * @return il punteggio ottenuto finora.
     */
    public int getPoints() {
        return points;
    }
    
    /**
     * Restituisce il nome del giocatore.
     * @return il nome del giocatore.
     */
    public String getName() {
        return name;
    }

    /**
     * Restituisce una descrizione del giocatore.
     * @return una stringa che rappresenta le caratteristiche del giocatore.
     */
    @Override
    public String toString() {
        return new StringBuilder()
                .append(getName())
                .append(" (")
                .append(getPoints())
                .append(" points, ")
                .append(stolenCardNo)
                .append(" cards stolen)")
                .toString();
    }

    /**
     * Restituisce le carte che compongono la mano del giocatore.
     * @return una stringa che rappresenta le carte del giocatore.
     */
    public String showHand() {
        StringBuilder handRepresentation = new StringBuilder();
        for(Card card : hand) {
            handRepresentation.append(card.toString()).append(' ');
        }
        return handRepresentation.toString();
    }
}