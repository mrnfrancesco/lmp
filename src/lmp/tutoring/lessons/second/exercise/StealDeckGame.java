package lmp.tutoring.lessons.second.exercise;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public final class StealDeckGame {
    
    private final int HAND_CAPACITY = 6;
    
    private final LinkedList<Player> players;
    private ArrayList<Card> table;
    private Deck deck;
    
    /**
     * Crea un gioco con due giocatori.
     * @param first il primo dei due giocatori.
     * @param second il secondo dei due giocatori.
     */
    public StealDeckGame(Player first, Player second) {
        players = new LinkedList<>();
        players.addFirst(first);
        players.addFirst(second);
        
        deck = new Deck();
        table = new ArrayList<>(deck.give(HAND_CAPACITY));
        distributeCards(HAND_CAPACITY);
    }
    
    /**
     * Distribuiamo a ogni giocatore le carte per la mano successiva.
     * @param howMany numero di carte da distribuire a ogni giocatore.
     */
    public void distributeCards(int howMany) {
        players.getFirst().takeCards(deck.give(howMany));
        players.getLast().takeCards(deck.give(howMany));
    }
    
    /**
     * Cambiamo l'ordine dei giocatori per il cambio del turno di gioco.
     */
    public void changeTurn() {
        /* spostiamo il primo giocatore in fondo alla lista */
        players.addLast(players.removeFirst());
    }
    
    /**
     * Mostra a video quali sono le carte in tavola.
     */
    public void showTable() {
        System.out.println("\nGame table is:");
        for(Card card : table) {
            System.out.print(card.toString() + ' ');
        }
        System.out.println();
    }
    
    /**
     * Permette di fare un turno di gioco.
     */
    public void playTurn() {
        /* mostriamo le carte del giocatore */
        System.out.println("\n> " + players.getFirst().getName() + "'s turn:");
        System.out.println(players.getFirst().showHand());
        System.out.print("What is your move [1-6]? ");
        
        /* prendiamo la carta che vuole giocare */
        Scanner responseReader = new Scanner(System.in);
        int response = responseReader.nextInt() - 1;
        Card choosenCard = players.getFirst().playCard(response);
        
        /* prendiamo le carte 'rubate' con quella scelta
         * cioè tutte le carte con lo stesso valore */
        ArrayList<Card> stolenCards = new ArrayList<>(0);
        for(int i = 0; i < table.size(); ++i) {
            Card card = table.get(i);
            if(choosenCard.compare(card) == 0) {
                /* rimuoviamo la carta dal tavolo di gioco */
                table.remove(card);
                /* inseriamo la carta nella lista delle carte 'rubate' */
                stolenCards.add(card);
            }
        }
        if(stolenCards.size() > 0) {
            /* assegnamo le carte 'rubate' al giocatore se ce ne sono */
            players.getFirst().raisePoint(stolenCards);
        }
        else {
            /* altrimenti aggiungiamo la carta nel tavolo di gioco */
            table.add(choosenCard);
        }
    }

    /**
     * Controlla che il gioco sia finito o meno.
     * @return true se il gioco è finito, false altrimenti.
     */
    public boolean isFinished() {
        return (!deck.hasNext());
    }
    
    /**
     * Restituisce il giocatore che ha vinto la partita.
     * @return il giocatore che ha vinto la partita oppure null in caso
     *          di pareggio.
     */
    public Player getWinner() {
        Player first = players.getFirst();
        Player second = players.getLast();
        Player winner = null;
        
        if(first.getPoints() > second.getPoints()) {
            winner = first;
        }
        else if(first.getPoints() < second.getPoints()) {
            winner = second;
        }
        
        return winner;
    }
}
