package lmp.tutoring.lessons.second.anonymousinnerclass;

import javax.swing.JOptionPane;

public class AnonymousInnerClassTest {
    
    public static void main(String[] args) {
        
        TalkingClock clock = new TalkingClock(1000);
        clock.start();
        
        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
    }
}
