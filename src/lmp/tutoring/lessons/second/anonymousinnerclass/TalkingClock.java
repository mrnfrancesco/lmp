package lmp.tutoring.lessons.second.anonymousinnerclass;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.Timer;

public class TalkingClock {
    
    private int interval;
    
    /**
     * Costruisce un orologio sonoro
     * @param interval intervallo di tempo tra i messaggi (in millisecondi)
     */
    public TalkingClock(int interval) {
        this.interval = interval;
    }
    
    public void start() {
        
        new Timer(interval, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Date now = new Date();
                System.out.println("the time is " + now);
            }
        }).start();
    }
}
