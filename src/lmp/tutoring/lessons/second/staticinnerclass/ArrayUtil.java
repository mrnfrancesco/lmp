package lmp.tutoring.lessons.second.staticinnerclass;

public abstract class ArrayUtil {
    
    public static Pair getMinMax(int[] values) {
        
        int max, min;
        max = min = values[0];
        
        for(int value : values) {
            if(value < min) {
                min = value;
            }
            else if(value > max) {
                max = value;
            }
        }
        
        return new Pair(min, max);
    }
    
    public static class Pair {
        
        private int first;
        private int second;
        
        public Pair(int first, int second) {
            this.first = first;
            this.second = second;
        }
        
        public int getFirst() {
            return first;
        }
        
        public int getSecond() {
            return second;
        }
    }
}
