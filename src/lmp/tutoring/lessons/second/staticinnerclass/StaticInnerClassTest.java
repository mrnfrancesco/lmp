package lmp.tutoring.lessons.second.staticinnerclass;

public class StaticInnerClassTest {
    
    public static void main(String[] args) {
        
        int[] values = new int[10];
        
        for(int i = 0; i < values.length; ++i) {
            values[i] = (int) (Math.random() * 100);
            System.out.print(values[i] + " ");
        }
        System.out.println();
        
        ArrayUtil.Pair minMax = ArrayUtil.getMinMax(values);
        System.out.println("the minimum values is: " + minMax.getFirst());
        System.out.println("the maximum values is: " + minMax.getSecond());
    }
    
}
