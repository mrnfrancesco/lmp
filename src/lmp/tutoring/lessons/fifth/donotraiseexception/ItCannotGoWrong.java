package lmp.tutoring.lessons.fifth.donotraiseexception;

public class ItCannotGoWrong {
    
    public static void main(String[] args) {
        
        if(args.length != 2) {
            System.err.println("Too few/many params specified. "
                    + "Need iteration number and a string to print.");
            return;
        }
        
        int iterationCounter;
        try {
            iterationCounter = Integer.parseInt(args[0]);
        } catch(NumberFormatException e) {
            System.err.println(args[0] + " is not a valid iteration counter.");
            return;
        }
        
        String toPrint = args[1];
        
        for(int i = 0; i < iterationCounter; ++i) {
            System.out.print(toPrint);
        }
        System.out.println();
    }
}