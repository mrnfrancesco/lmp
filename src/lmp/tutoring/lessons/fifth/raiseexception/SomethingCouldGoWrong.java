package lmp.tutoring.lessons.fifth.raiseexception;

public class SomethingCouldGoWrong {
    
    public static void main(String[] args) {
        
        /*
         * Quanti modi trovate per mandare in crash questo codice?
         * Ce ne sono almeno 3 che dovreste individuare :P
         */
        
        int passedInteger = Integer.parseInt(args[0]);
        String toPrint = args[1];
        
        for(int i = 0; i < passedInteger; ++i) {
            System.out.print(toPrint);
        }
        System.out.println();
    }
}