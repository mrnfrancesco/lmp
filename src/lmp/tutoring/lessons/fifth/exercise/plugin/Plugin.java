package lmp.tutoring.lessons.fifth.exercise.plugin;

import java.util.prefs.Preferences;

public class Plugin {

    private static final String PREFERENCES_NODE = "mrnfrancesco";
    private static final String PLUGIN_PKG_KEY = "on.start.plugin";
    
    public static void main(String[] args) {
        
        Preferences preferences = Preferences.userRoot().node(PREFERENCES_NODE);
        
        String animalType = preferences.get(PLUGIN_PKG_KEY, null);
        
        if(animalType == null) {
            animalType = Cat.class.getName();
            preferences.put(PLUGIN_PKG_KEY, animalType);
        }
        
        try {
            Animal animal = (Animal) Class.forName(animalType).newInstance();
            animal.doVerse();
        } catch(ClassCastException e) {
            System.err.println(
                    "Cannot run plugin: " + animalType + 
                    "\nIt must implement Animal interface."
            );
        } catch(ReflectiveOperationException e) {
            System.err.println(
                    "Selected plugin (" + animalType + ")" +
                    " is not compliant with specification.\n" +
                    "See documentation for more detail."
            );
            
        }
    }
    
}
