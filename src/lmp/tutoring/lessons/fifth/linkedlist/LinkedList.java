package lmp.tutoring.lessons.fifth.linkedlist;

public class LinkedList {
    
    /*
     * Riusciamo a correggere tutti gli errori e rendere l'applicazione
     * più sicura?
     */
    
    private final Node head;
    private int size;
    
    public LinkedList() {
        head = new Node();
        size = 0;
    }

    public Node getFirstNode () {
        return head.getNext();
    }

    public void insertFirst(Node node) {
        node.setNext(head.getNext());
        insert(head, node);
    }
    
    public void insertLast (Node node) {
        Node currentNode = head;
        Node lastNode = head;
        while (currentNode != null) {
            lastNode = currentNode;
            currentNode = currentNode.getNext();
        }
        insert(lastNode, node);
    }
    
    private void insert(Node parent, Node child) {
        parent.setNext(child);
        ++size;
    }

    public void removeNode (Node deadNode) {
        if(deadNode == null) {
            return;
        }
        
        Node currentNode = head;
        Node lastNode = head;
        while (currentNode != null || currentNode.equals(deadNode)) {
            lastNode = currentNode;
            currentNode = currentNode.getNext();
        }
        remove(lastNode, currentNode);
    }

    private void remove(Node lastNode, Node currentNode) {
        lastNode.setNext(currentNode.getNext());
    }
    
    public int size () {
        return size;
    }
    
    public void print() {
        Node currentNode = head.getNext();
        while (currentNode != null) {
            System.out.println(currentNode.getInfo());
            currentNode = currentNode.getNext();
        }
    }

}
