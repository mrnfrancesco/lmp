package lmp.tutoring.lessons.fifth.linkedlist.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import lmp.tutoring.lessons.fifth.linkedlist.Node;

public class NodeTest {
    
    public static void main(String[] args) throws Exception {

        for(Method method : NodeTest.class.getDeclaredMethods()) {
            if(method.getName().equals("main")) {
                continue;
            }
            try {
                System.out.print(method.getName());
                method.invoke(null);
                System.out.println(": passed");
            } catch(InvocationTargetException e) {
                System.out.println(": not passed ["
                        + e.getCause().getMessage() + "]");
            }
        }
        
    }
    
    public static void constructorsTest() {
        Node node;
        
        node = new Node();
        assert node != null : "new Node() get null";
        
        node = new Node("qualsiasi oggetto");
        assert node != null : "new Node(Object) get null";
    }
    
    public static void getInfoTest() {
        String expectedValue = "campo info";
        Node node = new Node(expectedValue);
        String returnedValue = (String) node.getInfo();
        
        assert expectedValue.equals(returnedValue) : "getInfo() returns wrong value";
    }
    
    public static void nextTest() {
        Node head = new Node();
        
        Node expectedValue = null;
        Node returnedValue = head.getNext();
        
        assert returnedValue == expectedValue : "getNext() returns  " + 
                returnedValue + " (" + expectedValue + " expected)";
        
        expectedValue = new Node();
        head.setNext(expectedValue);
        returnedValue = head.getNext();
        
        assert returnedValue == expectedValue : "setNext() sets  " + 
                returnedValue + " (" + expectedValue + " expected)";
    }
}
