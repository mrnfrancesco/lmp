package lmp.tutoring.lessons.fifth.linkedlist;

public class LinkedListTest {
    
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.insertFirst(new Node(5));
        list.insertFirst(new Node(10.12345));
        list.insertFirst(null);
        list.insertLast(new Node("Una stringa anche è un oggetto"));
        list.print();
    }
}
