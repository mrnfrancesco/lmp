package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;


public abstract class Scheduler {
  
    protected final ArrayList<Job> jobs;
    
    /* Intendiamo con schedule il metodo di schedulazione, cioè una sorta di
     * ordinamento dei job per cui il prossimo da eseguire è il primo della
     * lista. Quando chiamiamo schedule, quindi, devono essere anche eseguiti
     * tutti i job. */
    public abstract void schedule();
    
    /* Assegniamo allo scheduler una lista di job da schedulare ed eseguire */
    public Scheduler(ArrayList<Job> jobs) {
        /* Evitiamo che vengano generate eccezioni dovute a puntatori nulli */
        if(jobs == null) {
            jobs = new ArrayList<>();
        }
        this.jobs = jobs;
    }
    
    protected void run(long duration) {
        Job job = jobs.get(0);
        System.out.print("JOB " + job.getId() + ": STARTED");
        job.run(duration);
        if(job.isTerminated()) {
            System.out.print(" -> TERMINATED");
            jobs.remove(job);
            System.out.println(" -> REMOVED");
        }
        else if(job.isBlocked()) {
          System.out.println(" -> BLOCKED (remains " + job.getDuration() + ")");
        }
    }
    
    protected void run() {
        run(jobs.get(0).getDuration());
    }
    
}
