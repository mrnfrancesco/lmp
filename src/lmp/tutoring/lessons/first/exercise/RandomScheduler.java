package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;
import java.util.Collections;

public class RandomScheduler extends Scheduler {
    
    public RandomScheduler(ArrayList<Job> jobs) {
        super(jobs);
    }
    
    @Override
    public void schedule() {
        /* il metodo di schedulazione Random cambia casualmente l'ordine
         * dell'insieme di job, poi semplicemente li esegue tutti in ordine. */
        Collections.shuffle(jobs);
        while(!jobs.isEmpty()) {
            run();
        }
    }
}
