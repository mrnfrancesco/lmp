package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;
import java.util.Collections;

public class LIFOScheduler extends Scheduler {
    
    public LIFOScheduler(ArrayList<Job> jobs) {
        super(jobs);
    }

    @Override
    public void schedule() {
        /* il metodo di schedulazione LIFO inverte l'ordine dell'insieme
         * di job, poi semplicemente li esegue tutti in ordine. */
        Collections.reverse(jobs);
        while(!jobs.isEmpty()) {
            run();
        }
    }
}
