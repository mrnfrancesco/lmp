package lmp.tutoring.lessons.first.exercise;

public class Job {
    /* Ogni job può vedere questo attributo (perché è static) */
    private static long nextID = 1L;
    /* Questo ID invece è visibile solo alla specifica istanza della classe */
    private final long id;
    /* Indica quanto tempo serve al job per terminare la computazione */
    private long duration;
    /* Indica lo stato in cui si trova il Job */
    private State state;
    /* Indica il nome del programma che è stato lanciato che ha creato il job */
    private final String name;
    
    /* Crea un job assegnandogli un ID incrementale e lo pone come READY */
    public Job(String name, long duration) {
        state = State.READY;
        this.name = name;
        this.duration = duration;
        id = nextID;
        /* OGNI oggetto della classe Job vedrà questo incremento */
        nextID = nextID + 1;
    }
    
    /* Simula l'esecuzione del job */
    public void run(long duration) {
        /* eseguiamo la simulazione solo se il job non è già terminato */
        if(duration > 0 && state != State.TERMINATED) {
            /* se si cerca di avviare il job per più tempo di quello richiesto
             * si esegue tutto il job */
            if(duration > this.duration) {
                duration = this.duration;
            }
            state = State.EXECUTION;
            try {
                /* Fermiamo l'esecuzione del programma per la durata richiesta*/
                Thread.sleep(duration);
                this.duration = this.duration - duration;
            }
            catch(InterruptedException e) {
                /* Eccezione "silenziata" (le vedremo più in là) */
            }
            /* Impostiamo lo stato in funzione del fatto che il job abbia
             * terminato o meno tutta la sua esecuzione */
            if(this.duration == 0) {
                state = State.TERMINATED;
            }
            else {
                state = State.BLOCKED;
            }
        }
    }
    
    /* Fornisce l'ID che è stato assegnato al Job corrente */
    public long getId() {
        return id;
    }
    /* Fornisce il quantitativo di tempo che il job deve ancora spendere
     * per completare la sua esecuzione */
    public long getDuration() {
        return duration;
    }
    
    /* Ci dice se il job è terminato oppure no */
    public boolean isTerminated() {
        return (state == State.TERMINATED);
    }
    /* Ci dice se il job è stato creato, ma mai eseguito */
    public boolean isReady() {
        return (state == State.READY);
    }
    /* Ci dice se il job è stato eseguito, ma non è terminato */
    public boolean isBlocked() {
        return (state == State.BLOCKED);
    }
    
}
