package lmp.tutoring.lessons.first.exercise;

public enum State {
    READY,
    EXECUTION,
    BLOCKED,
    TERMINATED
}
