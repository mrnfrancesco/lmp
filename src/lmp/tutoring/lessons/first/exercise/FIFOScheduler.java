package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;

public class FIFOScheduler extends Scheduler {

    public FIFOScheduler(ArrayList<Job> jobs) {
        super(jobs);
    }
    
    @Override
    public void schedule() {
        /* il metodo di schedulazione FIFO non modifica l'ordine dell'insieme
         * di job, ma semplicemente li esegue tutti in ordine. */
        while(!jobs.isEmpty()) {
            run();
        }
    }

    
}
