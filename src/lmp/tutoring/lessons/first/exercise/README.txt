Come esercizio vogliamo sviluppare un simulatore di uno scheduler che può
utilizzare più politiche di scheduling; in questo esempio abbiamo implementato
cinque tipologie di scheduling: FIFO, LIFO, Round Robin e due politiche
inventate "Random" e "Fully Random" (vedi il codice per i dettagli sul
funzionamento della politica di scheduling).

Ogni job da schedulare avrà questi campi:
- Nome del processo che ha creato il job
- ID univoco del job
- Durata, cioè quanto tempo macchina server per completare il job
- Stato, cioè lo stato corrente in cui si trova il job in ogni momento

I job devono essere creati randomicamente dal main.
Lo scheduler deve prendere tutti i job creati ed eseguirli secondo la propria
politica di schedulazione.