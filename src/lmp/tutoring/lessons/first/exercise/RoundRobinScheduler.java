package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;

public class RoundRobinScheduler extends Scheduler {

    public RoundRobinScheduler(ArrayList<Job> jobs) {
        super(jobs);
    }
    
    @Override
    public void schedule() {
        /* il metodo di schedulazione FullyRandom scambia casualmente l'ordine
         * dell'insieme di job, poi ne esegue uno con una durata casuale e
         * poi ricomincia da capo */
        while(!jobs.isEmpty()) {
            /* porta il primo elemento in fondo alla lista */
            Job job = jobs.get(0);
            jobs.remove(job);
            jobs.add(job);
            /* esegue i primi 500 millisecondi del job scelto */
            run(500L);
        }
    }
}
