package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;

public class SchedulerTest {
    
    public static void main(String[] args) {
        
        /* Generiamo casualmente l'insieme di 10 job */
        ArrayList<Job> jobs = new ArrayList<>();
        for(int i = 0; i < 10; ++i) {
            /* stabilisce casualmente una durata del job nell'intervallo */
            long duration = (long) (1 + Math.random() * (6000 - 1));
            jobs.add(new Job("process_" + i, duration));
        }
        /* istanzia uno scheduler ed esegue tutti i job secondo la politica
         * che è propria dello scheduler stesso 
         * (istanziate voi quello che più vi piace) */
        RoundRobinScheduler fifoScheduler = new RoundRobinScheduler(jobs);
        fifoScheduler.schedule();
    }
}
