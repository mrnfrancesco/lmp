package lmp.tutoring.lessons.first.exercise;

import java.util.ArrayList;
import java.util.Collections;

public class FullyRandomScheduler extends Scheduler {
    
    public FullyRandomScheduler(ArrayList<Job> jobs) {
        super(jobs);
    }
    
    @Override
    public void schedule() {
        /* il metodo di schedulazione FullyRandom scambia casualmente l'ordine
         * dell'insieme di job, poi ne esegue uno con una durata casuale e
         * poi ricomincia da capo */
        while(!jobs.isEmpty()) {
            Collections.shuffle(jobs);
            /* genera un numero casuale nell'intervallo [500, 5000] */
            long randomDuration = (long) (500 + Math.random() * (5000 - 500));
            run(randomDuration);
        }
    }
}
