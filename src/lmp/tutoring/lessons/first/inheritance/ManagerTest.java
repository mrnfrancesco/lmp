package lmp.tutoring.lessons.first.inheritance;

import java.util.GregorianCalendar;

public class ManagerTest {
    
    public static void main(String[] args) {
        
        Manager boss = new Manager("Carl Cracker",
                                   80000,
                                   1987,
                                   GregorianCalendar.NOVEMBER,
                                   15
        );
        boss.setBonus(5000);
        
        Employee[] staff = new Employee[2];
        staff[0] = new Employee("Harry Hacker",
                                50000,
                                1989,
                                GregorianCalendar.OCTOBER,
                                1
        );
        staff[1] = new Employee("Tommy Tester",
                                40000,
                                1990,
                                GregorianCalendar.FEBRUARY,
                                15
        );
     
        for(Employee employee : staff) {
            System.out.println(employee.toString());
        }
        
        System.out.println(boss.toString());
        System.out.println(boss.getName() +" hire day is: "+ boss.getHireDay());
    }
}
