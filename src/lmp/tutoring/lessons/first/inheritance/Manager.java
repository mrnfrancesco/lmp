package lmp.tutoring.lessons.first.inheritance;

public class Manager extends Employee {
    
    private double bonus;
    
    public Manager(String name, double salary, int year, int month, int day) {
        super(name, salary, year, month, day);
        bonus = 0;
    }
    
    @Override
    public double getSalary() {
        return super.getSalary() + bonus;
    }
    
    public void setBonus(double bonus) {
        if(bonus < 0) {
            this.bonus = 0;
        }
        else {
            this.bonus = bonus;
        }
    }
    
    @Override
    public String toString() {
        return super.toString() + " (bonus: " + bonus + ")";
    }
}
