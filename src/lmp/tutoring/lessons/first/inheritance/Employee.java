package lmp.tutoring.lessons.first.inheritance;

import java.util.GregorianCalendar;

public class Employee {
    
    private final String name;
    private double salary;
    private final GregorianCalendar hireDay;
    
    public Employee(String name, double salary, int year, int month, int day) {
        this.name = name;
        this.salary = salary;
        hireDay = new GregorianCalendar(year, month, day);
    }
    
    public void raiseSalary(double byPercent) {
        double raise = salary * byPercent / 100;
        salary += raise;
    }
    
    public String getName() {
        return name;
    }
    
    public double getSalary() {
        return salary;
    }
    
    public String getHireDay() {
        return hireDay.get(GregorianCalendar.YEAR) +
                "-" + (hireDay.get(GregorianCalendar.MONTH) + 1) +
                "-" + hireDay.get(GregorianCalendar.DAY_OF_MONTH);
    }
    
    @Override
    public String toString() {
        return "name: " + name + "\tsalary: " + salary;
    }
}
