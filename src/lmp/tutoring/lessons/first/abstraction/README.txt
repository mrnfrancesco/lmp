Questo package non contiene alcun metodo main. Lo scopo è quello di mostrare
come il meccanismo di ereditarietà permetta di creare gerarchie strutturate ad
albero.

Quella che siamo andati a costruire, in particolare, è strutturata in questo
modo:

                SHAPE
               /     \
              /       \
           CIRCLE   RECTANGLE
                       |
                     SQUARE

A voi l'esercizio di sfruttare questi oggetti per costruire un main che ne
verifichi le proprietà e le potenzialità!