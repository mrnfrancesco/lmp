package lmp.tutoring.lessons.first.abstraction;

import java.awt.Point;

public abstract class Shape {
    
    private Point position; // shape's coordinate in a 2D space
    
    public Shape(int x, int y) {
        position = new Point(x, y);
    }

    public void moveTo(int x, int y) {
        position.setLocation(x, y);
    }
    
    public void relativeMoveTo(int deltaX, int deltaY) {
        position.translate(deltaX, deltaY);
    }
    
    public void relativeMoveToXAxis(int deltaX) {
        relativeMoveTo(deltaX, 0);
    }
    
    public void relativeMoveToYAxis(int deltaY) {
        relativeMoveTo(0, deltaY);
    }
    
    public Point getPosition() {
        return position;
    }
    
    public abstract double getPerimeter();
    public abstract double getArea();
}
