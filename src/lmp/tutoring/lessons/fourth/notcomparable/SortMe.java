package lmp.tutoring.lessons.fourth.notcomparable;

public class SortMe {
    
    public final static int MIN_VALUE = 0;
    public final static int MAX_VALUE = 1000;
    
    private int value;

    public SortMe(int value) {
        /*
         * come risolviamo questo (pericolosissimo) warning?
         * ci sono almeno 4 modi! :P
         */
        setValue(value);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        /* Impostiamo il parametro all'interno del range stabilito */
        if(value < MIN_VALUE) {
            value = MIN_VALUE;
        } else if(value > MAX_VALUE) {
            value = MAX_VALUE;
        }
        
        this.value = value;
    }
    
}
