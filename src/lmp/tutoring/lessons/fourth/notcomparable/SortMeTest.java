package lmp.tutoring.lessons.fourth.notcomparable;

import java.util.Arrays;

public class SortMeTest {

    public static void main(String[] args) {

        SortMe[] array = new SortMe[]{
            new SortMe(1),
            new SortMe(5),
            new SortMe(2),
            new SortMe(3),
            new SortMe(7),
            new SortMe(4),
            new SortMe(9999)
        };

        /* stampiamo tutti i valori così come sono */
        for (SortMe element : array) {
            System.out.print(element.getValue() + " ");
        }
        System.out.println();
        
        /* E se li volessimo ordinati? */
        Arrays.sort(array); // Errore! perché?
    }
}
