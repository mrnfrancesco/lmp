package lmp.tutoring.lessons.fourth.iterable;

import java.util.Iterator;

public class DoubleArrayWrapper implements Iterable<Double> {
    
    private final static int MIN_INDEX = 0;
    
    private final double[] array;
    
    public DoubleArrayWrapper(int size) {
        array = new double[size];
    }
    
    public int size() {
        return array.length;
    }
    
    public int validateIndex(int i) {
        if(i < MIN_INDEX) {
            return MIN_INDEX;
        } else if(i >= array.length) {
            return array.length - 1;
        } else {
          return i;  
        }
    }
    
    public double get(int i) {
        return array[validateIndex(i)];
    }
    
    public void set(int i, double value) {
        array[validateIndex(i)] = value;
    }

    @Override
    public Iterator<Double> iterator() { // metodo dell'interface Iterable
        
        return new Iterator<Double>() {

            private int nextIndex = 0;
            
            @Override
            public boolean hasNext() { // metodo dell'interface Iterator
                return (nextIndex < array.length);
            }

            @Override
            public Double next() { // metodo dell'interface Iterator
                return array[nextIndex++];
            }

            @Override
            public void remove() { // metodo dell'interface Iterator
                /* non necessario, ce lo dice il Javadoc! */
            }
        };
    }
}
