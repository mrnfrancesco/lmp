package lmp.tutoring.lessons.fourth.notiterable;

public class DoubleArrayWrapperTest {
    
    public static void main(String[] args) {
        
        DoubleArrayWrapper daWrapper = new DoubleArrayWrapper(3);
        daWrapper.set(-912837, 1.234);
        daWrapper.set(1, 2.345);
        daWrapper.set(128312, 3.456);
        System.out.println("l'array ha " + daWrapper.size() + " elementi.");
        
        /* e se volessimo scorrere l'array per stamparlo? come possiamo fare? */
        /*
        for (double elem : daWrapper) { // ERRORE! il wrapper non è iterabile!
            System.out.print(elem + " ");
        }
        System.out.println();
        */
    }
}
