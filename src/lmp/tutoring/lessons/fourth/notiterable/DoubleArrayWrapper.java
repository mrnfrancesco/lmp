package lmp.tutoring.lessons.fourth.notiterable;

public class DoubleArrayWrapper {
    
    private final static int MIN_INDEX = 0;
    
    private final double[] array;
    
    public DoubleArrayWrapper(int size) {
        array = new double[size];
    }
    
    public int size() {
        return array.length;
    }
    
    public double get(int i) {
        if(i < MIN_INDEX) {
            return array[MIN_INDEX];
        } else if(i >= array.length) {
            return array[array.length - 1];
        } else {
          return array[i];  
        }
    }
    
    /* come facciamo a scrivere meno codice duplicato? */
    public void set(int i, double value) {
        if(i < MIN_INDEX) {
            array[MIN_INDEX] = value;
        } else if(i >= array.length) {
            array[array.length - 1] = value;
        } else {
          array[i] = value;
        }
    }
}
