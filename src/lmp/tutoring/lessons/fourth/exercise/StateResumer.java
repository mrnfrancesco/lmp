package lmp.tutoring.lessons.fourth.exercise;

import java.util.prefs.Preferences;

public class StateResumer {
    
    private final static String LAST_STATE_KEY = "my_last_state";
    
    public static void main(String[] args) {
        
        if(args.length != 1) {
            System.err.println("Inserire esattamente un parametro");
            System.exit(0);
        }
        
        Preferences preferences = Preferences.userRoot().node("stateResumer");
        String lastState = preferences.get(LAST_STATE_KEY, null);
        if(lastState != null) {
            System.out.println("L'ultima volta ci siamo lasciati con " +
                    lastState);
            System.out.println("Ora invece ci lasciamo con " + args[0]);
        } else {
            System.out.println("Ciao! È la prima volta che ci vediamo :)");
            System.out.println("La prossima volta mi ricorderò: " + args[0]);
        }
        
        preferences.put(LAST_STATE_KEY, args[0]);
    }
}
