package lmp.tutoring.lessons.fourth.exercise;

import java.util.Map;

public class InformationRetriever {

/* Che ne dite di mettere un po' di indentazione? Però con stile ;) */
public static void main(String[] args) {
/* mostriamo tutte le informazioni che riusciamo a scovare! */
for (Map.Entry<Object, Object> property
: System.getProperties().entrySet()) {

System.out.println(property.getKey() + " : " + property.getValue());
}
System.out.println();

System.out.println("Ciao " + System.getProperty("user.name") + ", "
+ "visto quante cose so su di te? :)");
}
}
