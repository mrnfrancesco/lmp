package lmp.tutoring.lessons.fourth.exercise.plugin;

import java.util.prefs.Preferences;

public class Plugin {

    private static final String PREFERENCES_NODE = "mrnfrancesco";
    private static final String PLUGIN_PKG_KEY = "on.start.plugin";
    
    public static void main(String[] args) throws Exception {
        
        Preferences preferences = Preferences.userRoot().node(PREFERENCES_NODE);
        
        String animalType = preferences.get(PLUGIN_PKG_KEY,Dog.class.getName());
        /* versione con reflection per esempio di: Animal animal = new Dog(); */
        Animal animal = (Animal) Class.forName(animalType).newInstance();
        animal.doVerse();
    }
    
}
