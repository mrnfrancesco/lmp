package lmp.tutoring.lessons.fourth.comparable;

public class SortMe implements Comparable<SortMe> {
    
    public final static int MIN_VALUE = 0;
    public final static int MAX_VALUE = 1000;
    
    private int value;

    public SortMe(int value) {
        setValue(value);
    }

    public int getValue() {
        return value;
    }
    
    public final void setValue(int value) {
        /* Impostiamo il parametro all'interno del range stabilito */
        if(value < MIN_VALUE) {
            value = MIN_VALUE;
        } else if(value > MAX_VALUE) {
            value = MAX_VALUE;
        }
        
        this.value = value;
    }

    @Override
    public int compareTo(SortMe other) {
        if(this.value > other.value) {
            return 1;
        } else if(this.value < other.value) {
            return -1;
        } else {
            return 0;
        }
    }  
    
}
