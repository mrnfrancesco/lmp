package lmp.tutoring.lessons.third.exercise.third;

public class Node {
    
    private Node next;
    private final Object info;
   
    public Node () {
        this(null);
    }
    
    public Node(Object info) {
        this.info = info;
        next = null;
    }
    
    public void setNext(Node node) {
        next = node;
    }

    public Node getNext () {
        return next;
    }
   
    public Object getInfo() {
        return info;
    }
}
