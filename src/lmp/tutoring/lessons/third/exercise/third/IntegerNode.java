package lmp.tutoring.lessons.third.exercise.third;

public class IntegerNode extends Node {

    public IntegerNode(Integer info) {
        super(info);
    }

    @Override
    public Integer getInfo() {
        return (Integer) super.getInfo();
    }
}
