package lmp.tutoring.lessons.third.exercise.third;

public class LinkedListTest {
    
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.insertFirst(new IntegerNode(5));
        list.insertFirst(new IntegerNode(6));
        list.insertLast(new Node("Una stringa anche è un oggetto"));
        list.print();
    }
}
