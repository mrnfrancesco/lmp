package lmp.tutoring.lessons.third.exercise;

public class First {
    
    private int foo;
    private long bar;
    
    public First(int foo, long bar) {
        this.foo = foo;
        this.bar = bar;
    }
    
    public int getFoo() {
        return foo;
    }
    
    public long getBar() {
        return bar;
    }
}
