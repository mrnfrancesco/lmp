package lmp.tutoring.lessons.third.exercise;

import java.io.File;
import java.io.FilenameFilter;

public class Second {
    
    public static void main(final String[] args) {
        
        if(args.length < 1 || args.length > 2) {
            System.out.println("/path/to/directory [filter]");
            System.exit(0);
        }
        
        File dir = new File(args[0]);
        if(!dir.exists() || !dir.isDirectory()) {
            System.out.println("The specified directory does not exists (" +
                                    dir.getAbsolutePath() + ")"
            );
            System.exit(0);
        }
        
        String[] filesName;
        if(args.length == 1) {
            filesName = dir.list();
        }
        else {
            filesName = dir.list(new FilenameFilter() {
                
                @Override
                public boolean accept(File parentDir, String filename) {
                    return filename.contains(args[1]);
                }
            });
        }
        
        for (String fileName : filesName) {
            System.out.println(fileName);
        }
    }
}
