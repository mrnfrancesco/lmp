package lmp.tutoring.lessons.third.datastructureindependence;

import java.util.ArrayList;
import java.util.Arrays;

public class WhichList {
    
    public static void main(String[] args) {
        
        /* Un modo complicato (?) e puramente didattico di creare una lista */
        ArrayList<Number> numbers = new ArrayList<>(
                Arrays.asList(
                        new Number[] {
                            4,
                            5.4321,
                            9999999999L,
                            54321.678F
                        }
                )
        );
        
        /* Equivalente a scrivere: */
        // ArrayList<Number> numbers = new ArrayList<>();
        // numbers.add(4);
        // numbers.add(5.4321);
        // numbers.add(9999999999L);
        // numbers.add(54321.678F);
        
        print(numbers);
        print(311005L);
        System.out.println();
        
        /* che succede però se ora invece di ArrayList avessimo una lista di
         * tipo Queue o Dequeue o LinkedList o . . . ? Dovremmo riscivere tutti
         * i metodi print ognuno con il parametro di tipo diverso?
         * No! usiamo il polimorfismo! */
    }
    
    /* Usiamo l'overloading per stampare sia un singolo elemento che una
     * intera lista */
    private static void print(ArrayList<Number> lst) {
        for (Number number : lst) {
            print(number);
            System.out.print(" ");
        }
        System.out.println();
    }
    
    private static void print(Number elem) {
            if(elem instanceof Integer) {
                System.out.print((Integer) elem);
            }
            else if(elem instanceof Double) {
                System.out.print((Double) elem + "D");
            }
            else if(elem instanceof Long) {
                System.out.print((Long) elem + "L");
            }
            else if(elem instanceof Float) {
                System.out.print((Float) elem + "F");
            }
    }
}
