package lmp.tutoring.lessons.third.datastructureindependence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SmartWichList {
    
    public static void main(String[] args) {
        
        /* Un modo complicato (?) e puramente didattico di creare una lista */
        List<Number> numbers = new ArrayList<>(
                Arrays.asList(
                        new Number[] {
                            4,
                            5.4321,
                            9999999999L,
                            54321.678F
                        }
                )
        );
        /* Stampiamo una lista implementata tramite ArrayList */
        print(numbers);
        System.out.println();
        
        /* Stampiamo una lista implementata tramite LinkedList */
        numbers = new LinkedList<>();
        numbers.add(4);
        numbers.add(5.4321);
        numbers.add(9999999999L);
        numbers.add(54321.678F);
        
        print(numbers);
        System.out.println();
    }
    
    /* Usiamo l'overloading per stampare sia un singolo elemento che una
     * intera lista */
    private static void print(List<Number> lst) {
        for (Number number : lst) {
            print(number);
            System.out.print(" ");
        }
        System.out.println();
    }
    
    private static void print(Number elem) {
            if(elem instanceof Integer) {
                System.out.print((Integer) elem);
            }
            else if(elem instanceof Double) {
                System.out.print((Double) elem + "D");
            }
            else if(elem instanceof Long) {
                System.out.print((Long) elem + "L");
            }
            else if(elem instanceof Float) {
                System.out.print((Float) elem + "F");
            }
    }
}
