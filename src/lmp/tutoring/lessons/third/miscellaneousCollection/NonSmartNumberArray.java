package lmp.tutoring.lessons.third.miscellaneousCollection;

public class NonSmartNumberArray {
    
    public static void main(String[] args) {
        
        int foo = 5;
        double bar = 9.1234;
        
        /* ERRORE! la conversione implicita da double a int può portare a
         * perdita di precisione. */
        //int implicitIntegerFoobar[] = new int[] {foo, bar};
        
        /* OK! se convertiamo esplicitamente stiamo dicendo che non ci interessa
         * la perdita di precisione... ma noi non vogliamo perderla! */
        int explicitIntegerFoobar[] = new int[] {foo, (int)bar};
        print(explicitIntegerFoobar);
        
        /* OK! nessun errore e nessuna perdita di informazione... ma come
         * facciamo a sapere poi quale valore era inizialmente intero e quale
         * invece era double? non possiamo! ma lo vogliamo */
        double doubleFoobar[] = new double[] {foo, bar};
        print(doubleFoobar);
    }
    
    /* Implementiamo un metodo per la stampa rapida degli elementi di un
     * array di interi . . . */
    private static void print(int[] array) {
        for(int elem : array) {
            System.out.print(elem + " ");
        }
        System.out.println();
    }
    /* . . . e se vogliamo lo stesso anche con un array di double?
     * perdiamo informazione!
     * in più creiamo tantissimo codice duplicato!! */
    private static void print(double[] array) {
        for(double elem : array) {
            System.out.print(elem + " ");
        }
        System.out.println();
    }
}
