package lmp.tutoring.lessons.third.miscellaneousCollection;

public class SmartNumberArray {
    
    public static void main(String[] args) {
        
        /* Warning dell'IDE: Unnecessary boxing to integer */
        //Integer foo = new Integer(5);
        
        /* Così abbiamo lo stesso risultato di prima, ma con un enorme vantaggio
         * sia Integer che Double sono classi con una superclasse comune!
         * Possiamo usare il polimorfismo! */
        Integer foo = 5;
        Double bar = 9.1234;
        
        /* Non perdiamo precisione nei valori, ma ora abbiamo un array di
         * Number e non sappiamo più quale sottoclasse particolare è ogni
         * valore . . . è veramente così? no! */
        Number[] foobar = new Number[] {foo, bar};
        print(foobar);
        
        /* Dato che il boxing è implicito nelle classi wrapper, allora possiamo
         * fare anche cose del tipo: */
        Number[] numbers = new Number[] {
            4, // = new Integer(4);
            5.4321, // = new Double(5.4321);
            9999999999L,// = new Long(9999999999L) = new Long((long)9999999999);
            54321.678F // = new Float(54321.678) = new Float((float) 54321.678);
        };
        print(numbers);
    }

    /* Inoltre in questo modo scriviamo una sola volta il metodo print() */
    private static void print(Number[] array) {
        for(Number elem : array) {
            if(elem instanceof Integer) {
                System.out.print((Integer) elem);
            }
            else if(elem instanceof Double) {
                System.out.print((Double) elem + "D");
            }
            else if(elem instanceof Long) {
                System.out.print((Long) elem + "L");
            }
            else if(elem instanceof Float) {
                System.out.print((Float) elem + "F");
            }
            System.out.print(" ");
        }
        System.out.println();
    }
}
